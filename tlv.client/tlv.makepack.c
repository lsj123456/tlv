/*********************************************************************************
 ****      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ****                  All rights reserved.
 ****
 ****       Filename:  tlv.makepack.c
 ****    Description:  This file is tlv client 
 ****                 
 ****        Version:  1.0.0(2019年03月31日)
 ****         Author:  liushoujin <2398866942@qq.com>
 ****      ChangeLog:  1, Release initial version on "2019年03月31日 13时32分00秒"
 ****                 
 ***********************************************************************************/

#include "tlv.client.h"
#include "tlv.crc.h"

#define HEAD 0xFD
#define ID 0X01
#define INT_TIME 0X02
#define FLOAT_TEMP 0X03

int makepack(char *buf,int size)
{
    int id_len=0;
    int data_len=0;
    int time_len=0;
    int temp_len=0;
    int buf_place=0;
    int  i;
    char *id="lsj";
    char datatime[32];
    float temp;
    unsigned short crc16=0;

    if(!buf || size<18)
    {
        printf("Invalid input arguements\n");
        return 0;
    }
    buf[buf_place]=HEAD;
    buf_place+=1;

    data_len=11;
    buf[buf_place]=data_len;
    buf_place+=1;

    buf[buf_place]=ID;
    buf_place+=1;
    id_len=strlen(id);
    memcpy(&buf[buf_place],id,id_len);
    buf_place+=id_len;
    
    buf[buf_place]=INT_TIME;
    buf_place+=1;
    memset(datatime,0,sizeof(datatime));
    int time=getdatatime(datatime);
    time_len=time;
    printf("time_len is %d\n",time_len);
    for(int i=0;i<time_len;i++,buf_place++)
        buf[buf_place]=datatime[i];

    buf[buf_place]=FLOAT_TEMP;
    buf_place+=1;
    ds18b20_get_temperature(&temp);
    int temper_p1;
    int temper_p2;
    temper_p1=(int)temp;
    temper_p2=(int)((temp-temper_p1)*100);
    buf[buf_place]=temper_p1;
    buf_place+=1;
    buf[buf_place]=temper_p2;
    buf_place+=1;

    crc16=crc_itu_t(MAGIC_CRC,buf,buf_place);
    ushort_to_bytes(&buf[buf_place],crc16);
    buf_place+=2;

    for(i=0;i<buf_place;i++)
    {
        printf("0x%02x ",buf[i]);
    }
    printf("\n");
    printf("buf_place is %d",buf_place);
}
