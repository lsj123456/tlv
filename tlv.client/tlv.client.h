/*********************************************************************************
 ***      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ***                  All rights reserved.
 ***
 ***       Filename:  tlv.client.h
 ***    Description:  This file is tlv sclient 
 ***                 
 ***        Version:  1.0.0(2019年04月04日)
 ***         Author:  liushoujin <2398866942@qq.com>
 ***      ChangeLog:  1, Release initial version on "2019年04月04日 16时38分00秒"
 ***                 
 **********************************************************************************/
#include<stdio.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <time.h>
#include <signal.h>
#include<getopt.h>
#include<netinet/in.h>
#include<netdb.h>
#include <libgen.h>

int ds18b20_get_temperature(float *temp);
//int makepack_temp(char *buf,int size);
int getdatatime(char *datatime);
//int makepack_time(char *buf,int size);
void dump_buf(char *data, int len);
//int makepack_id(char *buf,int size);
int makepack(char *buf,int size);
int connect_server(char *ip,int port);
void print_usage(const char *program_name);
void sign_handler(int signum);
void dns(char *domain_name,char **ip);
