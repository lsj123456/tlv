/* ********************************************************************************
 *****      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 *****                  All rights reserved.
 *****
 *****       Filename:  tlv.crc.h
 *****    Description:  This file is tlv client 
 *****                 
 *****        Version:  1.0.0(2019年4月2日)
 *****         Author:  liushoujin <2398866942@qq.com>
 *****      ChangeLog:  1, Release initial version on "2019年4月2日 18时23分00秒"
 *****                 
 *************************************************************************************/
#ifndef CRC_ITU_T_H
#define CRC_ITU_T_H

#define MAGIC_CRC           0x1E50

extern const unsigned short  crc_itu_t_table[256];

extern int ushort_to_bytes(unsigned char *bytes, unsigned short val);
extern unsigned short bytes_to_ushort(unsigned char *bytes, int len);

extern unsigned short crc_itu_t(unsigned short crc, const unsigned char *buffer, unsigned int len);

static inline unsigned short crc_itu_t_byte(unsigned short crc, const unsigned char data)
{
		return (crc << 8) ^ crc_itu_t_table[((crc >> 8) ^ data) & 0xff];
}

#endif /* TLV.CRC.H */

