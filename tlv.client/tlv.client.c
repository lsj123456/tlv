/*********************************************************************************
 ***      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ***                  All rights reserved.
 ***
 ***       Filename:  tlv.client.c
 ***    Description:  This file is tlv client 
 ***                 
 ***        Version:  1.0.0(2019年04月04日)
 ***         Author:  liushoujin <2398866942@qq.com>
 ***      ChangeLog:  1, Release initial version on "2019年04月04日 16时37分52秒"
 ***                 
 **********************************************************************************/
#include"tlv.client.h"

int g_stop=0;

void print_usage(const char *program_name)
{
    printf("%s --(2019.4.4)\n",program_name);
    printf("%s -i (server_ip) -p (server_port) -h (help)\n",program_name);
    printf("-i --ip   the ip of tlv server\n");
    printf("-p --port   the port of tlv serveer\n");
    printf("-h --help   the tlv_client how to use\n");
    return;
}

void sign_handler(int signum)
{
    g_stop=1;
    /*if(SIGTERM==signum)
    {   
        printf("SIGTERM signal detected\n");
        g_stop=1; 
    }   
    else if(SIGALRM==signum)
    {
        printf("SIGALRM signal detected\n");
        g_stop=1;
    }
    else if(SIGINT==signum)
    {
        printf("SIGINT signal detected\n");
        g_stop=1;
    }*/
}

void dns(char *domain_name,char **ip)
{
    struct hostent       *server_name=NULL;

    server_name=gethostbyname(domain_name);
    inet_ntop(server_name->h_addrtype,server_name->h_addr,*ip,32);
}

int main(int argc,char **argv)
{
    char           *program_name;
    int            opt=-1;
    char           *ip=NULL;
    char           *servip=(char *)malloc(32);
    int            port=0;
    struct sockaddr_in servaddr;
    int            rv=-1;
    int            server_fd=-1;
    char           buf[128];
    int            bytes;    
    
    program_name=basename(argv[0]);
    const char *short_opts="i:p:h";
    const struct option long_opts[]={
        {"ip",required_argument,NULL,'i'},
        {"port",required_argument,NULL,'p'},
        {"help",no_argument,NULL,'h'},
        {0,0,0,0}
    };

while((opt=getopt_long(argc,argv,short_opts,long_opts,NULL))!=-1)
{
    switch(opt)
    {
        case 'i':
            ip=optarg;
            dns(ip,&servip);
            break;
        case 'p':
            port=atoi(optarg);
            break;
        case 'h':
            print_usage(program_name);
            return 0;
    }
}                  
if(!servip ||  !port)
{
    print_usage(program_name);
    return 0;
}

server_fd = socket( AF_INET, SOCK_STREAM, 0); 
if(server_fd < 0)
{   
    printf("Fail to create a server socket [%d]: %s\n", server_fd, strerror(errno) );
    return -1; 
}  
printf("creat a server socket[%d] successufully!\n",server_fd);
memset(&servaddr, 0, sizeof(servaddr));
servaddr.sin_family = AF_INET;
servaddr.sin_port = htons(port);
servaddr.sin_addr.s_addr=htonl(INADDR_ANY);

signal(SIGTERM,sign_handler);
signal(SIGALRM,sign_handler);
signal(SIGINT,sign_handler);

while(!g_stop)
{
    memset(buf,0,sizeof(buf));
    bytes=makepack(buf,sizeof(buf));
    dump_buf(buf,bytes);
   
    if(rv<0)
    {
        rv=connect(server_fd,(struct sockaddr*)&servaddr,sizeof(servaddr));
        if(rv<0)
        {
            printf("connect to server [%s:%d] failure:%s\n",servip,port,strerror(errno));
            close(rv);
            break;
        }
        printf("connect to server [%s:%d] successfully \n",servip,port);
    }

    rv=write(server_fd,buf,strlen(buf));
    if(rv<0)
    {
        printf("write to server by server_fd [%d] failure:%s\n",server_fd,strerror(errno));
        return -1;
        break;
    }
    else if(rv>0)
    {
        printf("write bytes to server by server_fd [%d] successfully\n",server_fd);
    }
    else if(rv==0)
    {
         printf("server_fd [%d] get disconnect\n",server_fd);
         return -1;
    }

        sleep(30);
}   
free(servip);
close(server_fd);
printf("program strat stop running...\n");
return 0;
}

