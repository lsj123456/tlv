/*********************************************************************************
 *      Copyright:  (C) 2019 LingYun<lingyun@email.com>
 *                  All rights reserved.
 *
 *       Filename:  tlv.get.time.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(01/04/19)
 *         Author:  LingYun <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "01/04/19 21:21:09"
 *                 
 ********************************************************************************/

#include"tlv.client.h"

int getdatatime(char *datatime)
{
    time_t seconds;
    struct tm *pTM;

    time(&seconds);
    pTM=localtime(&seconds);
    /*sprintf(datatime,"%04d%02d%02d%02d%02d%02d",pTM->tm_year,pTM->tm_mon+1,pTM->tm_mday,pTM->tm_hour,pTM->tm_min,pTM->tm_sec);*/
    datatime[0]=pTM->tm_year;
    datatime[1]=pTM->tm_mon+1;
    datatime[2]=pTM->tm_mday;
    datatime[3]=pTM->tm_hour;
    datatime[4]=pTM->tm_min;
    datatime[5]=pTM->tm_sec;
    return 6; 
}

