/*********************************************************************************
 *      Copyright:  (C) 2019 liushoujin <2398866942@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  tlv.get.time.c
 *    Description:  This file tlv server 
 *                 
 *        Version:  1.0.0(01/04/19)
 *         Author:  liushoujin <2398866942@qq.com>
 *      ChangeLog:  1, Release initial version on "01/04/19 21:21:09"
 *                 
 ********************************************************************************/

#include"tlv.server.h"

int getdatatime(char *datatime)
{
    time_t seconds;
    struct tm *pTM;

    time(&seconds);
    pTM=localtime(&seconds);
    sprintf(datatime,"%4d%2d%2d%2d%2d%2d",pTM->tm_year,pTM->tm_mon+1,pTM->tm_mday,pTM->tm_hour,pTM->tm_min,pTM->tm_sec);
    return 0; 
}

