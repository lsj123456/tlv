/*********************************************************************************
 ******      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ******                  All rights reserved.
 ******
 ******       Filename:  tlv.dump.buf.h
 ******    Description:  This file is tlv server
 ******                 
 ******        Version:  1.0.0(2019年4月2日)
 ******         Author:  liushoujin <2398866942@qq.com>
 ******      ChangeLog:  1, Release initial version on "2019年4月2日 19时27分53秒"
 ******                 
 ***************************************************************************************/

#define CHARS_PER_LINE 16
static char *print_char =
"                "
"                "
" !\"#$%&'()*+,-./"
"0123456789:;<=>?"
"@ABCDEFGHIJKLMNO"
"PQRSTUVWXYZ[\\]^_"
"`abcdefghijklmno"
"pqrstuvwxyz{|}~ "
"                "
"                "
" ???????????????"
"????????????????"
"????????????????"
"????????????????"
"????????????????"
"????????????????";                                                                
