/*********************************************************************************
 ***      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ***                  All rights reserved.
 ***
 ***       Filename:  tlv.server.h
 ***    Description:  This file is tlv server 
 ***                 
 ***        Version:  1.0.0(2019年04月04日)
 ***         Author:  liushoujin <2398866942@qq.com>
 ***      ChangeLog:  1, Release initial version on "2019年04月04日 16时38分00秒"
 ***                 
 **********************************************************************************/
#include<stdio.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<time.h>
#include<signal.h>
#include<getopt.h>
#include<netinet/in.h>
#include<netdb.h>
#include<libgen.h>
#include<sys/epoll.h>
#include<ctype.h>
#include<sys/time.h>
#include<sys/resource.h>
#include<sqlite3.h>
#include<syslog.h>

int ds18b20_get_temperature(char *temp);
int getdatatime(char *datatime);
void dump_buf(char *data, int len);
int makepack(char *buf,int size);
int dividepack(char *buf,int size);
int connect_server(char *ip,int port);
void print_usage(const char *program_name);
void sign_handler(int signum);
void dns(char *domain_name,char **ip);
