/*********************************************************************************
 ****      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ****                  All rights reserved.
 ****
 ****       Filename:  tlv.dividepack.c
 ****    Description:  This file is tlv server 
 ****                 
 ****        Version:  1.0.0(2019年04月04日)
 ****         Author:  liushoujin <2398866942@qq.com>
 ****      ChangeLog:  1, Release initial version on "2019年04月05日 12时19分52秒"
 ****                 
 ************************************************************************************/

#include "tlv.server.h"
#include "tlv.crc.h"

#define HEAD 0xFD
#define ID 0X01
#define INT_TIME 0X02
#define FLOAT_TEMP 0X03
static sqlite3 *db;

int dividepack(char *buf,int size)
{
     int     i;
     unsigned short crc16=0;
     char    user[32];
     int     year;
     int     month;
     int     day;
     int     hour;
     int     minute;
     int     second;
     int     temp_1;
     int     temp_2;
     int     rc;
     char    *db_name="TEMP.db";
     char    *error=0;
     char    *insert=(char*)malloc(256);
     char    *select="select * from TEMP;";
     char    *sql="CREATE TABLE IF NOT EXISTS TEMP(""ID INT PRIMARY KEY,"
                                                    "USER CHAR(10),"
                                                    "TIME CHAR(50),"
                                                    "TEMP CHAR(10));";

     memset(user,0,sizeof(user));

     if(!buf||size<=6)
     {
         printf("Invalid input arguements\n");
         return 0;
     }

flag1:if(size>6)
     {
    flag2:for(i=0;i<size;i++)
         {
             if(buf[i]==HEAD)
             {
                 if(size-i>5)
                 {
                     if(buf[i+1]==11)
                     {
                         if(buf[i+2]==ID)
                         {
                             memcpy(user,&buf[i+3],3);
                             printf("user is %s\n",user);
                             if(buf[i+6]==INT_TIME)
                             {
                                 year=buf[i+7];
                                 year+=1900;
                                 printf("year is %d\n",year);
                                 month=buf[i+8];
                                 printf("month is %d\n",month);
                                 day=buf[i+9];
                                 printf("day is %d\n",day);
                                 hour=buf[i+10];
                                 printf("hour is %d\n",hour);
                                 minute=buf[i+11];
                                 printf("minute is %d\n",minute);
                                 second=buf[i+12];
                                 printf("second is %d\n",second);
                                 if(buf[i+13]==FLOAT_TEMP)
                                 {
                                     temp_1=buf[i+14];
                                     printf("temp_1 is %d\n",temp_1);
                                     temp_2=buf[i+15];
                                     printf("temp_2 is %d\n",temp_2);
                                    
                                     crc16=crc_itu_t(MAGIC_CRC,buf,18);
                                     //ushort_to_bytes(&buf[i+16],crc16);
                                     if(crc16==0)
                                     {   
                                         printf("crc true\n");
                                         printf("divide pack finish\n");
                                         memmove(buf,&buf[i],size);size=0;
                                     }
                                     else
                                     {
                                         memmove(buf,&buf[i+16],size-i-16);size=size-i-16;goto flag1;
                                    }
                                 }
                                 else 
                                 {
                                    memmove(buf,&buf[i+13],size-i-13);size=size-i-13;goto flag1;
                                 }
                             }
                             else 
                             {
                                 memmove(buf,&buf[i+6],size-i-6);size=size-i-6;goto flag1;
                             }
                         }
                         else 
                         {
                             memmove(buf,&buf[i+2],size-i-2);size=size-i-2;goto flag1;
                         }
                     }
                         else
                         {
                             memmove(buf,&buf[i+1],size-i-1);size=size-i-1;goto flag1;
                         }
                    }
                 else 
                 {
                     memmove(buf,&buf[i],size-i);return size-i;goto flag1;
                 }
              }
             else
             {
                 goto flag2;
             }
         }
     }
     else 
     {
         return size;
     }

     rc=sqlite3_open(db_name,&db);
     if(rc!=SQLITE_OK)
     {
        fprintf(stderr,"sql error:%s\n",sqlite3_errmsg(db));
     }
     else
     {
         printf("open sqlite successfully!\n");
     }
    

     rc=sqlite3_exec(db,sql,NULL,NULL,&error);
     if(rc!=SQLITE_OK)
     {
         fprintf(stderr,"sql error:%s\n",error);
         sqlite3_close(db);
         sqlite3_free(error);
         exit(1);
     }
     else
     {
         printf("create table successfully!\n");
     }

     snprintf(insert,256,"insert into TEMP(USER,TIME,TEMP)values('%s','%04d-%02d-%02d %02d:%02d:%02d','%d.%d');",user,year,month,day,hour,minute,second,temp_1,temp_2);
     rc=sqlite3_exec(db,insert,NULL,NULL,&error);
     if(rc!=SQLITE_OK)
     {
         fprintf(stderr,"sql error:%s\n",error);
         sqlite3_close(db);
         sqlite3_free(error);
         exit(1);
     }
     else
     {
         printf("insert successfully!\n");
     }
     
     rc=sqlite3_exec(db,select,NULL,NULL,&error);
     if(rc!=SQLITE_OK)
     {
         fprintf(stderr,"sql error:%s\n",error);
         sqlite3_close(db);
         sqlite3_free(error);
         exit(1);
     }
     else
     {
         printf("select successfully!\n");
     }
     sqlite3_close(db);
}
