/*********************************************************************************
 ***      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
 ***                  All rights reserved.
 ***
 ***       Filename:  tlv.get.temp.c
 ***    Description:  This file is tlv server
 ***                 
 ***        Version:  1.0.0(2019年03月31日)
 ***         Author:  liushoujin <2398866942@qq.com>
 ***      ChangeLog:  1, Release initial version on "2019年03月31日 12时47分00秒"
 ***                 
 **********************************************************************************/
#include "tlv.server.h"

int ds18b20_get_temperature(char *temp)
{
    char           w1_path[50]="/sys/bus/w1/devices/";
    char           chip[20];
    int            found=0;
    DIR            *dirp;
    struct dirent  *direntp;
    char           buf[128];
    int            fd=-1;
    char           *str;
    if((dirp=opendir(w1_path))==NULL)
    {
        printf("apendir error:%s\n",strerror(errno));
	return -1;
    }
    while((direntp=readdir(dirp))!=NULL)
    {
        if(strstr(direntp->d_name,"28-"))
	{
	    strcpy(chip,direntp->d_name);
	    found=1;
	    break;
	}
    }
    closedir(dirp);
    if(!found)
    {
        printf("can not find ds18b20 in %s\n",w1_path);
	return -1;
    }
    strncat(w1_path,chip,sizeof(w1_path)-strlen(w1_path));
    strncat(w1_path,"/w1_slave",sizeof(w1_path)-strlen(w1_path));
    if((fd=open(w1_path,O_RDONLY))<0)
    {
        printf("open %s error:%s\n",w1_path,strerror(errno));
	return -1;
    }
    memset(buf,0,sizeof(buf));
    if(read(fd,buf,sizeof(buf))<0)
    {
        printf("read %s error:%s\n",w1_path,strerror(errno));
	return -1;
    }
    str=strstr(buf,"t=");
    if(!str)
    {
        printf("error:can not get temperature\n");
	return -1;
    }
    str+=2;
    memcpy(temp,str,4);
    close(fd);
    return 0;
}
