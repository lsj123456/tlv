/*********************************************************************************
****      Copyright:  (C) 2019 liushoujin<2398866942@qq.com>
****                  All rights reserved.
****
****       Filename:  tlv.tlv.dump.buf.c
****    Description:  This file is tlv server 
****                 
****        Version:  1.0.0(2019年03月31日)
****         Author:  liushoujin <2398866942@qq.com>
****      ChangeLog:  1, Release initial version on "2019年4月2日 19时20分00秒"
****               
***********************************************************************************/

#include "tlv.server.h"
#include "tlv.dump.buf.h"

void dump_buf(char *data, int len)
{
    int rc;
    int idx;
    char prn[81];
    char lit[CHARS_PER_LINE + 1];
    char hc[4];
    short line_done = 1;

    rc = len;
    idx = 0;
    lit[CHARS_PER_LINE] = '\0';
    while (rc > 0)
    {
        if (line_done)
            snprintf(prn,81, "%08X: ", idx);
        do
    {                                                                                           
        unsigned char c = data[idx];                                                                
        snprintf(hc, 4, "%02X ", c);
        strncat(prn, hc, 4);
        lit[idx % CHARS_PER_LINE] = print_char[c];
            ++idx;
    } while (--rc > 0 && (idx % CHARS_PER_LINE != 0));

        line_done = (idx % CHARS_PER_LINE) == 0;
        if (line_done)
        printf("%s  %s\n", prn, lit);
        else if (rc == 0)
        strncat(prn, "   ", 81);
    }
    if (!line_done)
    {
        lit[(idx % CHARS_PER_LINE)] = '\0';
        while ((++idx % CHARS_PER_LINE) != 0)
        strncat(prn, "   ", 81);
        printf("%s  %s\n", prn, lit);
    }
}

